# MPA Network Scenario and Zone/Site Report Cards (Performance Measures)

 __Main author:__  Katie Gale  
 __Affiliation:__  Fisheries and Oceans Canada (DFO)   
 __Group:__        Marine Spatial Ecology and Analysis   
 __Location:__     Institute of Ocean Sciences   
 __Contact:__      e-mail: katie.gale@dfo-mpo.gc.ca | tel: 250-363-6411


This set of scripts will calculate the ecological performance measures at the scenario and zone/site level, outputting a series of csv files and a nicely-formatted PDF table ("Report Card")

- [Configuration file](#config)
- [Script 1: Setup](#script1)
- [Script 2: Calculate and output all metrics](#script2)
- [Script 3: Create PDF of report card](#script3)
- [Script 4: Clean up PDF outputs](#script4)
- [Script 5: Make PDF of Scenario of your choice](#script5)
- [Make scenario comparison figures](#comparefigures)


## Configuration file <a name="Configuration file"></a>
`Config_%date%.csv`
- For each scenario, specifies:
	- Scenario name and run date
	- Input file location (path)
	- Input file names
		- Specific to each scenario: coastline, existing area, FNCCP overlap, upland protection adjacency, attributes, spacing
		- Common to all scenarios, but need to be in each input folder: replicates, species categories, feature counts
	- Preferred title for PDFs 

## Script 1: Setup <a name="Script 1: Setup"></a>
`1_ReportCard_setup.R`
- Objective: Set parameters for creating Report Card/Performance Measure tables and PDF outputs
- Instructions: Set the following parameters:
	- Location of scripts
    - Name of configuration file
    - Name of scenario to run
    - Other parameters (cutoffs, areas, thresholds needed for next script)
    - Whether plots should be written and if coastline stats should be run
	
## Script 2: Calculate and output all metrics <a name="Script 2: Calculate and output all metrics"></a>
`2_ReportCard_PerformanceMeasures.R`
- Calculate all metrics and output csv files
- Instructions: 
	- Run Script 1 (`ReportCard_setup.R`)
	- Run this script without editing anything 

## Script 3: Create PDF of report card <a name="Script 3: Create PDF of report card"></a>
`3_MakeReportCard.Rmd`
- Objective: Convert outputs from Script 2 (`2_ReportCard_PerformanceMeasures.R`) into a PDF. 
	- The temp file scenarioToRun_temp.rdata specifies the scenario to run
	- The script looks in the CGA_Runs folder for the specified scenario, and pulls the inputs from the most recent version of the Scenario_Report_Card outputs. 
- Instructions: 
	- This script should be run right after Script 2 (i.e., if `scenarioToRun_temp.rdata` has not changed)
	- If Script 2 was not just run, if you're not sure what's in `scenarioToRun_temp.rdata`, or if you want to specify which scenario to run, use Script 5 (`5_runReportCard_fromExisting.R`)
	- Change the path to the config file in the header (in the R chunk called "title")
       	- Run this script without editing anything, using Ctrl-Shift-K in RStudio
	- Run Script 4 (`4_CleanUpReportCard.R`) after
**Note** on some installation configurations, Script 3 is not rendering the pdf properly. Use Script 5, see below for instructions.

## Script 4: Clean up PDF outputs <a name="Script 4: Clean up PDF outputs"></a>
`4_CleanUpReportCard.R`
- Objective: Cleans up the outputs of Script 3 (`3_MakeReportCard.Rmd`)
	- This script renames the generic-named output of Script 3 and puts a copy with the other csv outputs for that run
	- The temp file scenarioToRun_temp.rdata specifies the scenario to run
- Instructions: 
	- This script should be run right after running the .Rmd (Script 3) manually (i.e., if `scenarioToRun_temp.rdata` has not changed)
	- It's not necessary to run this if you used Script 5 (`5_runReportCard_fromExisting.R`)
	- Run this script without editing anything

## Script 5: Make PDF of Scenario of your choice <a name="Script 5: Make PDF of Scenario of your choice"></a>
`5_runReportCard_fromExisting.R`
- Objective: Make PDF from a scenario that you specify
	- Run this if Script 2 (`2_ReportCard_PerformanceMeasures.R`) was not just run, if you're not sure what's in `scenarioToRun_temp.rdata`, if you want to specify which scenario to run, or **if Script 2 doesn't render the pdf**
	- This makes a new temp file `scenarioToRun_temp.rdata` (the .Rmd needs it).
	- This runs the .Rmd from within the script, and outputs a LaTeX (.tex) file that can be run in TeXMaker to get the final pdf.
- Instructions: 
	- Specify the scenario and the location of the scripts and config file.
		- If running this without running Script 3, make sure to 
	- Run the script, and a correctly-named PDF should be created in the script folder.
	- Whether the pdf fails or succeeds, a .tex file will be created. This can be rendered into a pdf in TeXMaker (press F1 twice).
	- Edits can be made to the .tex file to adjust page breaks (add \newpage above the text to be carried to the new page).


## Make scenario comparison figures <a name="Make scenario comparison figures"></a>
`CompareScenarios_Figures.R`
- Objective: Makes plots from ScenarioReport output tables
- Instructions: 
	- Run on the outputs of Script 2 (`2_ReportCard_PerformanceMeasures.R`)

## **** NOTE - Final pdf formatting required
- Open resulting .tex file in texmaker
- replace "XCCCX" with "(" and "X333X" with ")".
- add "\newpage" above lines that you want to move to next page (section headings)
  - For Spatial N, add "\newpage" above line 429 (`\multicolumn{13}{l}{\textit{Birds - proportion (number) of features...}}\\`), and above (new) line 513 (`\multicolumn{13}{l}{\textit{Species features, by species group - proportion (number) meeting replication target}}\\`)
- re-render by pressing F1
