#Conservation Gaps Analysis Ecological Data Prep
#Katie Gale, 3 April 2018

#Join (a) table that a list of datasets and their associated functional/taxononomic group and/or species names
#to (b) table with consequence scores for human activity-functional group or -species interactions 

library(plyr)

# #Mock import data
# names<-data.frame(group=c(rep("Whales",4), rep("fish",2),"coral","coral"),species=c("grey","blue",'humpback',"orca","skate", "rockfish",NA,"redCoral"), data=c("greyd","blued","humpd","orcad","skated",'rockfishd',"corald","redCorald"))
# matrix<-data.frame(activity=c("aquaculture","fishing","recreation","recreation","recreation","recreation"), group=c("Whales","fish","fish","fish","coral","coral"), consequence=c("high","mid","low","mid","high","low"), species=c(NA,NA,NA,"rockfish",NA,"blueCoral"))

#Bring in Real Data
setwd("E:/Documents/Data files/MPATT Data/DataManagementCGA")

names<-read.csv("./interactionsMatrix_SpeciesLookup_20180327_kgedit_2018.03.29.csv")
matrix<-read.csv("./MPAT Interaction Matrix_Mar22_simplified_20180327.csv")

#Clean up column names
names(names)[1:5]<-c("group","species.name.dataset","species.name.essa","dataset","notes")
names<-names[,!names(names) %in% c("notes","X","X.1","X.2","X.3")]
names<-names[names$group!="",]
names$group<-tolower(names$group)

matrix<-matrix[,!names(matrix) %in% c("X","X.1","X.2","X.3","X.4","X.5","X.6","X.7","X.8","X.9")]
matrix<-matrix[matrix$group!="",]
matrix$group<-tolower(matrix$group)

#Fix some species names
matrix$species.name.essa<-tolower(matrix$species.name.essa)
matrix$species.name.essa<-gsub("\\s$*","",matrix$species.name.essa)
matrix$species.name.essa<-gsub("\\'","",matrix$species.name.essa)
matrix$species.name.essa<-gsub("\\'","",matrix$species.name.essa)
matrix$species.name.essa[matrix$species.name.essa=="quillback"]<-"quillback rockfish"
matrix$species.name.essa[matrix$species.name.essa=="darksplotched rockfish"]<-"darkblotched rockfish"
matrix$species.name.essa[matrix$species.name.essa=="NA"]<-NA

names$species.name.essa<-tolower(names$species.name.essa)
names$species.name.essa<-gsub("\\s$*","",names$species.name.essa)
names$species.name.essa<-gsub("\\'","",names$species.name.essa)
names$species.name.essa<-gsub("\\'","",names$species.name.essa)
names$species.name.essa[names$species.name.essa=="NA"]<-NA
names$species.name.essa[names$species.name.essa==""]<-NA

#check if names match between name table and interaction table 
unique(names$species.name.essa[!names$species.name.essa%in% matrix$species.name.essa]) #ok. Only hard and soft shelf are in species table and not matrix.
unique(matrix$species.name.essa[!matrix$species.name.essa%in% names$species.name.essa]) #species names in matrix but not in species table are not CPs, except Thayer's gull (ok - overlap with cali gull), barrows goldeneye (ok - overlap with harlequin duck), and shiner perch (no data)

#Remove the names in the matrix that aren't in the species table
rem<-unique(matrix$species.name.essa[!matrix$species.name.essa%in% names$species.name.essa]) 
matrix<-matrix[!matrix$species.name.essa%in% rem,]

#check if groups match
unique(names$group[!(names$group %in% matrix$group)]) #surfperches - ok, we don't have data anyway
unique(matrix$group[!(matrix$group %in% names$group)]) #only one of concern is macroalgae. Change it to Kelp bed like the others

matrix$group[matrix$group=="macroalgae"]<-"kelp bed and associated assemblages"

#First, set match type to be group, or to species if a name is provided
matrix$matchType<-"group"
matrix$matchType[!is.na(matrix$species.name.essa)]<-"species"
head(matrix)
matrix[200:250,]
subset(matrix, group=="hard shelf")
subset(matrix, group=="corals")
subset(matrix, group=="wetland and coastal birds")

#Merge data and set an index to keep track of individual lines
merged<-merge(matrix, names, by="group")
merged$index<-seq(1:nrow(merged))

merged$species.name.essa.x<-as.character(merged$species.name.essa.x)
merged$species.name.essa.y<-as.character(merged$species.name.essa.y)

#Remove lines where additional species have been attached to a species-specific record
#I.e., keep only lines where the species matches between the species table and the interaction table score, or where no species name was provided (i.e., group-level matches)
merged2<-subset(merged, is.na(species.name.essa.x) | species.name.essa.x==species.name.essa.y)
summary(as.factor(look$species.name.essa.x[look$matchType=="group"])) #all NA

# #Explore Duplicates (Multiple Scores for Same dataset-activity interaction)
# look<-merged2[,!names(merged2) %in% c("Sector", "Overall.Confidence.Score")] #Remove some columns to investigate
# look2<-subset(look, group=="baleen whales" &Sub.Activity=="Floating Raft/Floating Bag Suspension") #See here that matchType=species has a speciesname in speciesname.x, and matchtype.group does not
# look2$dupStatus<-NA
# duped<-look2$index[(duplicated(look2[,c("group","Activity","Sub.Activity","species.name.essa.y","dataset")]) | duplicated(look2[,c("group","Activity","Sub.Activity","species.name.essa.y","dataset")], fromLast=TRUE))]
# look2$dupStatus[look2$index %in% duped]<-"duped"
# look2 #See that each Humpback whale dataset is duplicated for aquaculture. There's one score from the baleen whale group match, and one from the species match. We want to keep the species match
# look3<-look2[is.na(look2$dupStatus) | (look2$dupStatus=="duped" & look2$matchType=="species"),] #keep only lines that aren't duplicated. If a line is duplicated, keep the species record
# look3 #Seems to work

#try removing duplicates on full dataset
merged2$dupStatus<-NA
duped<-merged2$index[(duplicated(merged2[,c("group","Sector","Activity","Sub.Activity","species.name.essa.y","dataset")]) | duplicated(merged2[,c("group","Sector","Activity","Sub.Activity","species.name.essa.y","dataset")], fromLast=TRUE))]
merged2$dupStatus[merged2$index %in% duped]<-"duped"
merged3<-merged2[is.na(merged2$dupStatus) | (merged2$dupStatus=="duped" & merged2$matchType=="species"),]

#check some example combinations before and after removal
subset(merged2, species.name.essa.y=="fin whale" & Sub.Activity=="Vessel Strikes") # can see fin whale has two scores, one from group and one from species. keep species
subset(merged3, species.name.essa.y=="fin whale" & Sub.Activity=="Vessel Strikes") # Kept only the species one

subset(merged2, species.name.essa.y=="long-tailed duck" & Sub.Activity=="Catastrophic oil spills") # can see long tailed ducks have two scores, one from group and one from species. keep species
subset(merged3, species.name.essa.y=="long-tailed duck" & Sub.Activity=="Catastrophic oil spills") # Kept only the species one

head(merged3) ##Looks good. Need to remove dataset-level duplicates
merged3<-merged3[,!names(merged3) %in% c("dupStatus")] #drop dup status

# subset(merged3, Sub.Activity=="Vessel Strikes" & group=="baleen whales")
# subset(merged3,Sub.Activity=="Bottom Trawling" &group %in% c("demosponges","glass sponges"))
# subset(merged3,dataset %in% c("eco_mammals_spermwhale_density_d"))
# subset(merged3,dataset %in% c("eco_birds_stormpetrel_colonies_d"))


#Re-categorize Consequence Scores
table(merged3$Consequence.Score, merged3$Consequence.Score.Num)
merged3$Consequence.Score.Num<-as.numeric(ifelse(merged3$Consequence.Score=="Major Negative", "-2", 
                                      ifelse(merged3$Consequence.Score=="Minor Negative", "-1",
                                             ifelse(merged3$Consequence.Score=="Negligible", "0",
                                                    ifelse(merged3$Consequence.Score=="Major negative", "-2","")))))

#Check for duplicates that occur when a dataset has more than one score due to a dataset being assigned to 2 groups or if one layer represents multiple species that all have scores
#find all duplicates for sector-activity-dataset, except where dataset is no data
dupeScores<-merged3[(duplicated(merged3[,c("Sector","Activity","Sub.Activity","dataset")]) | duplicated(merged3[,c("Sector","Activity","Sub.Activity","dataset")], fromLast=TRUE)) & merged3$dataset!="No data",]
unique(dupeScores$dataset[!names(dupeScores) %in% c("Sector","dupStatus")]) #61 datasets

#Tabulate how many datasets have multiple consequence scores
check<-ddply(dupeScores, c("Sector","Activity","Sub.Activity","dataset"), summarize, nScores=length(unique(Consequence.Score)))
diffScores<-subset(check, nScores>1) #if there are more than 1 score for each activity-dataset, we need to make it so there's only 1
unique(diffScores$dataset) #25 layers have duplicates

#explore these duplicate layers
subset(dupeScores, dataset=="eco_inverts_clambutter_records_d" & dupeScores$Sub.Activity=="Open Net Pens") # 1 layer assigned to 2 groups, score is attached to group
subset(dupeScores, dataset=="eco_birds_cormorant_density_m"& dupeScores$Sub.Activity=="Beach Seeding") #1 layer representing 4 species, score is attached to species and group
subset(dupeScores, dataset=="eco_birds_tuftedpuffin_colonies_d"& dupeScores$Sub.Activity=="Catastrophic oil spills") #1 layer representing 4 species, score is attached to species and group
subset(dupeScores, dataset=="eco_inverts_olympiaoyster_records_d"& Sub.Activity=="Open Net Pens") #1 layer representing 4 species, score is attached to species and group


#Sort out the duplicate scores. 
datasets<-unique(dupeScores$dataset) #62 species
dupeScores$combAct<-paste(dupeScores$Sector, dupeScores$Activity, dupeScores$Sub.Activity, sep="_")

keep<-0

#Loop through datasets. Within each dataset, for each activity, compare scores and choose one to carry forward
for (i in 1:length(datasets)){
  sub<-dupeScores[dupeScores$dataset==datasets[i],]
  Act<-unique(sub$combAct)

  for (a in 1:length(Act)){
    sub2<-sub[sub$combAct==Act[a],]

    #drop the less negative interaction (i.e., keep the more negative one). If both are the same score, keep the first record, 
    keep<-c(keep,min(sub2$index[sub2$Consequence.Score.Num==min(sub2$Consequence.Score.Num)]))
  }
}

#Set individual records to drop or keep
dupeScores$status<-"drop"
dupeScores$status[dupeScores$index %in% keep]<-"keep"
head(dupeScores)

dupeScores2<-dupeScores[,names(dupeScores) %in% c("dataset","combAct","Consequence.Score.Num","index","status", "Overall.Confidence.Score")]

# #explore
# subset(dupeScores2, dataset=="eco_inverts_clambutter_records_d" & dupeScores$combAct=="Aquaculture_Finfish Aquaculture_Open Net Pens") # 1 layer assigned to 2 groups, score is attached to group
# subset(dupeScores2, dataset=="eco_birds_cormorant_density_m"& dupeScores$combAct=="Aquaculture_Shellfish Aquaculture_Beach Seeding") #1 layer representing 4 species, score is attached to species and group
# subset(dupeScores2, dataset=="eco_birds_tuftedpuffin_colonies_d"& dupeScores$combAct=="Marine Energy_Oil and Gas_Catastrophic oil spills") #1 layer representing 4 species, score is attached to species and group
# subset(dupeScores2, dataset=="eco_inverts_olympiaoyster_records_d"& combAct=="Aquaculture_Finfish Aquaculture_Open Net Pens") #1 layer representing 4 species, score is attached to species and group

write.csv(dupeScores2,"./testDupeScores.csv",row.names=F) #this file shows which rows were dropped

#drop rows we don't want
dupeScores2drop<-dupeScores2$index[dupeScores$status=="drop"]
merged4<-merged3[!merged3$index %in% dupeScores2drop,]

nrow(merged3) #2816 starting
nrow(merged4) #2481 remain


#Replace Sub-Activity Names for matching to human use UID
#if na, set to Activity Name
merged4$Sub.Activity<-as.character(merged4$Sub.Activity)
merged4$Activity<-as.character(merged4$Activity)
merged4$Sub.Activity[is.na(merged4$Sub.Activity)]<-"null"
merged4$Sub.Activity[merged4$Sub.Activity=="null"]<-merged4$Activity[merged4$Sub.Activity=="null"]

#Replace non-unique names
merged4$Sub.Activity[merged4$Sector=="Commercial Fishing" & merged4$Sub.Activity=="Hook and Line"]<-"Hook and Line Com"
merged4$Sub.Activity[merged4$Sector=="Recreational Fishing" & merged4$Sub.Activity=="Hook and Line"]<-"Hook and Line Rec"
merged4$Sub.Activity[merged4$Sector=="Commercial Fishing" & merged4$Sub.Activity=="Mid-water gill-nets"]<-"Mid-water gill-nets Com"
merged4$Sub.Activity[merged4$Sector=="Recreational Fishing" & merged4$Sub.Activity=="Mid-water gill-nets"]<-"Mid-water gill-nets Rec"
merged4$Sub.Activity[merged4$Sector=="Commercial Fishing" & merged4$Sub.Activity=="Trap"]<-"Trap Com"
merged4$Sub.Activity[merged4$Sector=="Recreational Fishing" & merged4$Sub.Activity=="Trap"]<-"Trap Rec"
merged4$Sub.Activity[merged4$Sector=="Commercial Fishing" & merged4$Sub.Activity=="Trolling/Rod and Reel"]<-"Trolling/Rod and Reel Com"
merged4$Sub.Activity[merged4$Sector=="Recreational Fishing" & merged4$Sub.Activity=="Trolling/Rod and Reel"]<-"Trolling/Rod and Reel Rec"
merged4$Sub.Activity[merged4$Sector=="Commercial Fishing" & merged4$Sub.Activity=="Beach Seine/CasNets/Dip Nets"]<-"Beach Seine/CasNets/Dip Nets Com"
merged4$Sub.Activity[merged4$Sector=="Recreational Fishing" & merged4$Sub.Activity=="Beach Seine/CasNets/Dip Nets"]<-"Beach Seine/CasNets/Dip Nets Rec"
merged4$Sub.Activity[merged4$Sector=="Commercial Fishing" & merged4$Sub.Activity=="Intertidal Hand Picking/Digging"]<-"Intertidal Hand Picking/Digging Com"
merged4$Sub.Activity[merged4$Sector=="Recreational Fishing" & merged4$Sub.Activity=="Intertidal Hand Picking/Digging"]<-"Intertidal Hand Picking/Digging Rec"
merged4$Sub.Activity[merged4$Sector=="Commercial Fishing" & merged4$Sub.Activity=="SCUBA/Dive fishing"]<-"SCUBA/Dive fishing Com"
merged4$Sub.Activity[merged4$Sector=="Recreational Fishing" & merged4$Sub.Activity=="SCUBA/Dive fishing"]<-"SCUBA/Dive fishing Rec"
merged4$Sub.Activity[merged4$Activity=="Tidal Current" & merged4$Sub.Activity=="Wave energy"]<-"Tidal Wave Energy"

length(unique(paste0(merged4$Sub.Activity))) #75
length(unique(paste0(merged4$Sector, merged4$Activity,merged4$Sub.Activity)))#75

#Lose spaces and symbols
replace<-c("\\/|\\'|\\s|\\-|\\,|\\(|\\)")
merged4$Sector<-gsub(replace,"", merged4$Sector)
merged4$SectorCode<-ifelse(merged4$Sector=="Aquaculture","aq",
                           ifelse(merged4$Sector=="CommercialFishing","co",
                                  ifelse(merged4$Sector=="Hunting","hu",
                                         ifelse(merged4$Sector=="MarineEnergy","ma",
                                                ifelse(merged4$Sector=="OtherMarineActivities","ot",
                                                       ifelse(merged4$Sector=="Recreation","re",
                                                              ifelse(merged4$Sector=="RecreationalFishing","rf",
                                                                     ifelse(merged4$Sector=="TransportandNavigation","tr",""))))))))
summary(as.factor(merged4$SectorCode))
merged4$Activity<-gsub(replace,"", merged4$Activity)
merged4$Sub.Activity<-gsub(replace,"", merged4$Sub.Activity)

#Copy edits that are in Amos' HU table
merged4$Sub.Activity[merged4$Sub.Activity=="BoatBasedCommercialWildlifeViewing"]<-"BoatBasedCommWild"
merged4$Sub.Activity[merged4$Sub.Activity=="Seafloorelectricalcommunicationcables"]<-"Seafloorelectricalcommscables"
merged4$Sub.Activity[merged4$Sub.Activity=="Constructionofmarineinfrastructure"]<-"Constructionofmarineinfras"
merged4$Sub.Activity[merged4$Sub.Activity=="ShoreUseIntertidalExploration"]<-"ShoreUseIntertidalExplor"
merged4$Sub.Activity[merged4$Sub.Activity=="NonMotorizedRecreationalBoating"  ]<-"NonMotorizedRecBoating"
merged4$Sub.Activity[merged4$Sub.Activity=="Dumpingofdredgedsoilsandothermaterials"]<-"Dumpingofdredgedsoilsandother"
merged4$Sub.Activity[merged4$Sub.Activity=="IntertidalHandPickingDiggingCom"           ]<-"IntertidalHandPickDigCom"
merged4$Sub.Activity[merged4$Sub.Activity=="Sewageeffluentfromterrestrialoutfallareas"]<-"Sewageeffluentfromterrestrial"
merged4$Sub.Activity[merged4$Sub.Activity=="IntertidalHandPickingDiggingRec"]<-"IntertidalHandPickDigRec"
merged4$Sub.Activity[merged4$Sub.Activity=="Seafloorpipelinesfortransportingoilandgas" ]<-"Seafloorpipelinesfortransoil"
merged4$Sub.Activity[merged4$Sub.Activity=="Largevesselscargoandcruiseships"   ]<-"Largevesselscargo"
merged4$Sub.Activity[merged4$Sub.Activity=="MotorizedRecreationalBoating"   ]<-"MotorizedRecBoating"
merged4$Sub.Activity[merged4$Sub.Activity=="Windturbinesinshallowtransitionalanddeepwaters"   ]<-"Windturbinesinshallowtransitionaldeepwaters"
merged4$Sub.Activity[merged4$Sub.Activity=="Logstorageandhandling"   ]<-"Logstorageandhand"


#create UID and calculate length
merged4$UID<-tolower(paste("hu",merged4$SectorCode, merged4$Activity,merged4$Sub.Activity, "d",sep="_"))
merged4$len<-nchar(merged4$UID)
unique(merged4$Sub.Activity[merged4$len>65]) #None over 65 chars

#Get dataset name
merged4$dataset.name<-sapply(strsplit(as.character(merged4$dataset), "_"), "[",3)

merged5<-merged4[,c("index","species.name.dataset","dataset","dataset.name","Sector","SectorCode","Activity","Sub.Activity","UID","Consequence.Score","Overall.Confidence.Score")]
names(merged5)[2]<-"species.name"
head(merged5)

# #make sure all the UIDs in my sheet are in the hu_UID sheet
# hu<-read.csv("./mpatt_hu_UID_final_20180403_kg.csv")
# unique(merged5$UID[!(merged5$UID) %in% hu$UID]) #all good

write.csv(merged5, "E:/Documents/Data files/MPATT Data/DataManagementCGA/CGA_Interactions_2018.04.03_UID.csv", row.names=F)



